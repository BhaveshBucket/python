#exercise 1 to  in python

#this is command and to print strings 
'''print("Hello World")
print("Hello,","I am Bhavesh")
print("we can also use "+"+ to add two strings")'''
#for arithematic operatons
'''print((5+6/2+4/8*2)%3==0)
print(6+43>56)
print(10/3)'''
#variable assignment
'''var1=1
var2=23.32
var3='hello'
var4="hey"
print(var1+var2)
print(var3,var4)#spaced saparated string
print(var3+var4,"you")#concatenate string'''
#more variables
""" var1=123
var2="hello"
print(" i have a no.: %d and a string: %s"%(var1,var2))# , he use hoga variables ke beech
print(round(36387.23423)) """
#string and text more printing
""" s="name"
var="hello this is a string %d '%s' \"%s\"" % (1,s,s)

print(var)
print(s,var)#%r is for debugging; %s is for displaying
print("%s "%s)
print("%s "%s*int(10/2)) """
#printing
""" formatter = "%r %r %r %r"

print(formatter % (1, 2, 3, 4))
print(formatter % ("one", "two", "three", "four"))
print(formatter % (True, False, False, True))
print(formatter % (formatter, formatter, formatter, formatter))

months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

print("Here are the months: \n", months)

print(sffdsswdfwf #USE """ """ for multiple line printing
      kgdjsd
        lbnsdkhbf
            dhikfs
        vbsblsgv
      sfksfb
hagfsy) """#\t-tab \n-next line \non-split/next line 
#asking questions
""" st=input("Enter your name: ")
print(" hello ", st)
age=input("Enter your age: ")
print(age)
print("your weight ?")
w=input()
print(w) """

#parameters unpacking and variables
""" 
from sys import argv
script, first, second, third = argv #script me file name ayga(ex_1_to_10.py)

print("The script is called:", script)
print("Your first variable is:", first)
print("Your second variable is:", second)
print("Your third variable is:", third) """

#prompting and passing
""" 
from sys import argv
script, name = argv 
print("Your hobbies are:")
hobbies=input(">")
print("Hello:", script," your hobbies are :",hobbies)
 """
#reading files
""" 
 close—Closes the fi le. Like File- >Save.. in your editor.
 • read—Reads the contents of the fi le. You can assign the result to a variable.
 • readline—Reads just one line of a text file.
 • truncate—Empties the fi le. Watch out if you care about the file.
 • write(stuff)—Writes stuff to the file.
 """
""" from sys import argv

script, filename = argv

print("Here's your file %r" % filename)

txt = open(filename, 'w')#mode of file is read/ write/ append r+,w+,a+ for both read and write
#print(txt.read())
line=input("write something:")

#txt.truncate()
txt.write(line)

#print(txt.read())
txt.close() """

#more in files
""" from sys import argv
from os.path import exists

script, from_file, to_file=argv
print("Copying from %s to %s" % (from_file, to_file))
in_file=open(from_file)
indata=in_file.read()#object ko use karna sikho
print("Length of file :%d"%len(indata))
print("Does the output file exist:%r"%exists(to_file))

out_file=open(to_file,'w')
out_file.write(indata)
print("All set file copied !")
out_file.close()
in_file.close() """



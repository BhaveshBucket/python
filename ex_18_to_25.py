#name variable code functions
''' def print_var(arg1,arg2):        #def print_var(arg1,arg2): for more elements
    print("One variable is :%s %s"%(arg1,arg2))
    
def print_n(*args):#for n number of variables
    arg1, arg2 , arg3= args#duplicate names chalange
    print("arg1: %r, arg2: %r, arg3: %r" % (arg1, arg2, arg3))

print_var("Bhavesh","devesh")
print_n("Zed","Shaw","Mike") '''

#functions and files
''' from sys import argv
script, file_name=argv
def print_all(f):
    print(f.read())

#def rewind(f):
#    print(f.seek(0))
    
def print_a_line(line_count, f):
    print(line_count,f.readline())
    
current_file = open(file_name)

print_all(current_file)

#rewind(file_name)
current_line=1
print_a_line(current_line,current_file)
current_line+=1
print_a_line(current_line,current_file)
current_line+=1
print_a_line(current_line,current_file) '''

#function can return something
''' def add(a,b):
    return a+b

print("sum of 7 + 3 =",add(7,3)) '''

#more practice
''' This function will break up words for us '''
def break_words(stuff):
    words = stuff.split(' ')
    return words

''' Sorts the words. '''
def sort_words(words):
    return sorted(words)

''' Prints the first word after popping it off '''
def print_first_word(words):
    word = words.pop(0)
    print(word)
    
''' Prints the last word after popping it off '''
def print_last_word(words):
    word = words.pop(- 1)
    print(word)
    
''' Takes in a full sentence and returns the sorted words '''
def sort_sentence(sentence):
    words = break_words(sentence)
    return sort_words(words)

'''Prints the first and last words of the sentence'''
def print_first_and_last(sentence):
    words = break_words(sentence)
    print_first_word(words)
    print_last_word(words)
    
''' Sorts the words then prints the first and last one '''
def print_first_and_last_sorted(sentence):
    words = sort_sentence(sentence)
    print_first_word(words)
    print_last_word(words)
    
sentence = "All good things come to those who wait."
#words = break_words(sentence)
#print(words)
#sorted_word = sort_words(words)
#print(sorted_word)
#print_first_word(words)
#print_last_word(words)
sorted_word= sort_sentence(sentence)
print(sorted_word)
print_first_and_last(sentence)
print_first_and_last_sorted(sentence)